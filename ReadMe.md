# JavaScript Express Auth0
Simple Express API that integrates with Auth0.

- https://auth0.com/blog/node-js-and-express-tutorial-building-and-securing-restful-apis/

# Getting Started
- Clone this repo
- Install dependencies: `npm install`
- Start the API on port 5150 w/Nodemon: `npm start`

# Auth0 Domain
- https://manage.auth0.com/dashboard/us/dev-t2ndw215zcxbp5x6/tenant/custom_domains
- Tentant name ==> domain name ==> dev-t2ndw215zcxbp5x6.us.auth0.com

# Quick Start
- Auth0 provides the following code under the Quick Start tab:
```
var express = require('express');
var app = express();
var jwt = require('express-jwt');
var jwks = require('jwks-rsa');

var port = process.env.PORT || 8080;

var jwtCheck = jwt({
      secret: jwks.expressJwtSecret({
          cache: true,
          rateLimit: true,
          jwksRequestsPerMinute: 5,
          jwksUri: 'https://dev-t2ndw215zcxbp5x6.us.auth0.com/.well-known/jwks.json'
    }),
    audience: 'express-api',
    issuer: 'https://dev-t2ndw215zcxbp5x6.us.auth0.com/',
    algorithms: ['RS256']
});

app.use(jwtCheck);

app.get('/authorized', function (req, res) {
    res.send('Secured Resource');
});

app.listen(port);
```

# Auth0 Links
- https://auth0.com/docs/api/management/v2#!/Users/get_users
- https://auth0.com/docs/secure/tokens/access-tokens/get-management-api-access-tokens-for-production
- https://auth0.com/docs/secure/tokens/access-tokens/get-management-api-tokens-for-single-page-applications

# User ID
- The sub field in the JWT payload contains the UserId of the user:
```
{
  "iss": "https://dev-t2ndw215zcxbp5x6.us.auth0.com/",
  "sub": "zyLIJGNrkKm6oZhKurw3cBbLeyOE9VHO@clients",   <==== Right here
  "aud": "express-api",
  "iat": 1668627854,
  "exp": 1668714254,
  "azp": "zyLIJGNrkKm6oZhKurw3cBbLeyOE9VHO",
  "gty": "client-credentials"
}
```

Examples:
```
google-oauth2|107254062045555418667
auth0|637544ef67ad3e09030c82e1
```

# Auth0 Plans
![](./docs/auth0-plans.png)

# React Urls
![](./docs/react-urls.png)
